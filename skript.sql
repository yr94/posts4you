-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Jan 23, 2017 at 11:27 AM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `posts4u`
--

-- --------------------------------------------------------

--
-- Table structure for table `Comment`
--

CREATE TABLE `Comment` (
  `commentID` int(11) NOT NULL,
  `commentDay` int(11) NOT NULL,
  `commentMonth` int(11) NOT NULL,
  `commentYear` int(11) NOT NULL,
  `commentContent` varchar(255) NOT NULL,
  `userID` int(11) NOT NULL,
  `postID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Friendship`
--

CREATE TABLE `Friendship` (
  `friendshipID` int(11) NOT NULL,
  `friend1` int(11) NOT NULL,
  `friend2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Location`
--

CREATE TABLE `Location` (
  `locationID` int(11) NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Photo`
--

CREATE TABLE `Photo` (
  `postID` int(11) NOT NULL,
  `postDay` int(11) NOT NULL,
  `postMonth` int(11) NOT NULL,
  `postYear` int(11) NOT NULL,
  `photoText` varchar(255) NOT NULL,
  `photoURL` varchar(255) NOT NULL,
  `userID` int(11) NOT NULL,
  `locationID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Text`
--

CREATE TABLE `Text` (
  `postID` int(11) NOT NULL,
  `postDay` int(11) NOT NULL,
  `postMonth` int(11) NOT NULL,
  `postYear` int(11) NOT NULL,
  `textContent` varchar(255) NOT NULL,
  `userID` int(11) NOT NULL,
  `locationID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `userID` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `lastName` varchar(30) NOT NULL,
  `firstName` varchar(30) NOT NULL,
  `birthDay` int(11) NOT NULL,
  `birthMonth` int(11) NOT NULL,
  `birthYear` int(11) NOT NULL,
  `eMail` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `geschlecht` enum('m','w') DEFAULT NULL,
  `profilPicURL` varchar(255) DEFAULT NULL,
  `lives_in` int(11) DEFAULT NULL,
  `comes_from` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`userID`, `username`, `lastName`, `firstName`, `birthDay`, `birthMonth`, `birthYear`, `eMail`, `password`, `geschlecht`, `profilPicURL`, `lives_in`, `comes_from`) VALUES
(1, 'm', 'Iandovka', 'Mark', 1, 1, 2000, 'markiandovka@gmail.com', '$2y$10$kcbaSDz/22v.gt5jvmRsqOtugegZXgowJiUODTYAFX5Gmu8Etj0EG', NULL, NULL, NULL, NULL),
(2, 'yr', 'RÃ¼mmele', 'Yannick', 1, 1, 2000, 'yr@djs.ch', '$2y$10$KpRctknV.SYEEBcd05ATa.7u2n9bF2JCLN8ufW83G0GkJqqLq.RQq', NULL, NULL, NULL, NULL),
(4, '53', 'Iandovka', 'Mark', 1, 1, 2000, 'markiandovka@gmail.com', '$2y$10$PKA40UCjKU2Gn1z3U.Owt.nOtdxH4R2mCdes60w10vUM8K7FWV2Be', NULL, NULL, NULL, NULL),
(5, 'mark', 'Iandovka', 'Mark', 1, 1, 2000, 'markiandovka@gmail.com', '$2y$10$2vzLlFrkMNM2R1aG7LKn1uH/WhtPwaW.dUaKSQhxmnvsukdTKMKQi', NULL, NULL, NULL, NULL),
(6, 'unibas', 'Iandovka', 'Mark', 1, 1, 2000, 'markiandovka@gmail.com', '$2y$10$fLjSXs3pmudBPXE.PBUeGuaxtJylzZBFeJyUn0NToGQdUhqTElWte', 'w', NULL, NULL, NULL),
(7, 'yyy', 'Iandovka', 'Mark', 1, 1, 2000, 'markiandovka@gmail.com', '$2y$10$QJHkrbOAj4N9.tRpUl9XoOLDSSooFs0IVGPj906iUg9TwZ4BW2zLC', NULL, NULL, NULL, NULL),
(8, 'basel', 'Knowles', 'Beyonce', 1, 1, 2000, 'msa@jiofds.com', '7aaf6c53e9feb6c123b1db536cf7c544', NULL, NULL, NULL, NULL),
(10, 'loris', 'Sauter', 'Loris', 1, 1, 2000, 'ls@ls.com', '78a5cc24c4c2478c76b8d0afe772e231', NULL, NULL, NULL, NULL),
(11, 'mm', 'Iandovka', 'Mark', 1, 1, 2000, 'markiandovka@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Comment`
--
ALTER TABLE `Comment`
  ADD PRIMARY KEY (`commentID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `postID` (`postID`);

--
-- Indexes for table `Friendship`
--
ALTER TABLE `Friendship`
  ADD PRIMARY KEY (`friendshipID`),
  ADD KEY `friend1` (`friend1`),
  ADD KEY `friend2` (`friend2`);

--
-- Indexes for table `Location`
--
ALTER TABLE `Location`
  ADD PRIMARY KEY (`locationID`);

--
-- Indexes for table `Photo`
--
ALTER TABLE `Photo`
  ADD PRIMARY KEY (`postID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `locationID` (`locationID`);

--
-- Indexes for table `Text`
--
ALTER TABLE `Text`
  ADD PRIMARY KEY (`postID`),
  ADD KEY `userID` (`userID`),
  ADD KEY `locationID` (`locationID`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`userID`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `lives_in` (`lives_in`),
  ADD KEY `comes_from` (`comes_from`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Comment`
--
ALTER TABLE `Comment`
  MODIFY `commentID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Friendship`
--
ALTER TABLE `Friendship`
  MODIFY `friendshipID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Location`
--
ALTER TABLE `Location`
  MODIFY `locationID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Photo`
--
ALTER TABLE `Photo`
  MODIFY `postID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Text`
--
ALTER TABLE `Text`
  MODIFY `postID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Comment`
--
ALTER TABLE `Comment`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`),
  ADD CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`postID`) REFERENCES `Photo` (`postID`),
  ADD CONSTRAINT `comment_ibfk_3` FOREIGN KEY (`postID`) REFERENCES `Text` (`postID`);

--
-- Constraints for table `Friendship`
--
ALTER TABLE `Friendship`
  ADD CONSTRAINT `friendship_ibfk_1` FOREIGN KEY (`friend1`) REFERENCES `User` (`userID`),
  ADD CONSTRAINT `friendship_ibfk_2` FOREIGN KEY (`friend2`) REFERENCES `User` (`userID`);

--
-- Constraints for table `Photo`
--
ALTER TABLE `Photo`
  ADD CONSTRAINT `photo_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`),
  ADD CONSTRAINT `photo_ibfk_2` FOREIGN KEY (`locationID`) REFERENCES `Location` (`locationID`);

--
-- Constraints for table `Text`
--
ALTER TABLE `Text`
  ADD CONSTRAINT `text_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `User` (`userID`),
  ADD CONSTRAINT `text_ibfk_2` FOREIGN KEY (`locationID`) REFERENCES `Location` (`locationID`);

--
-- Constraints for table `User`
--
ALTER TABLE `User`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`lives_in`) REFERENCES `Location` (`locationID`),
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`comes_from`) REFERENCES `Location` (`locationID`);
