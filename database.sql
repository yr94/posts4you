CREATE TABLE Location(
	locationID int NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (locationID),
	city VARCHAR(50) NOT NULL,
	country VARCHAR(50) NOT NULL
);

CREATE TABLE User(
	userID int NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (userID),
	username VARCHAR(30) NOT NULL UNIQUE,
	lastName VARCHAR(30) NOT NULL,
	firstName VARCHAR(30) NOT NULL,
	birthDay int NOT NULL CHECK(birthDay BETWEEN 1 AND 31),
	birthMonth int NOT NULL CHECK(birthMonth BETWEEN 1 AND 12),
	birthYear int NOT NULL CHECK(birthYear > 1900),
	eMail VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,
	geschlecht ENUM('m', 'w'),
	profilPicURL VARCHAR(255) NOT NULL,
	lives_in int NOT NULL,
	FOREIGN KEY (lives_in) REFERENCES Location(locationID),
	comes_from int NOT NULL,
	FOREIGN KEY (comes_from) REFERENCES Location(locationID)
);
	
CREATE TABLE Friendship(
	friendshipID int NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (friendshipID),
	friend1 int NOT NULL,
	FOREIGN KEY (friend1) REFERENCES User(userID),
	friend2 int NOT NULL, 
	FOREIGN KEY (friend2) REFERENCES User(userID)
);

CREATE TABLE Text(
	postID int NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (postID),
	postDay int NOT NULL CHECK(postDay BETWEEN 1 AND 31),
	postMonth int NOT NULL CHECK(postMonth BETWEEN 1 AND 12),
	postYear int NOT NULL CHECK(postYear > 2015),
	textContent VARCHAR(255) NOT NULL,
	userID int NOT NULL,
	FOREIGN KEY (userID)
	REFERENCES User(userID),
	locationID int NOT NULL,
	FOREIGN KEY (locationID) REFERENCES Location(locationID)
);

CREATE TABLE Photo(
	postID int NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (postID),
	postDay int NOT NULL CHECK(postDay BETWEEN 1 AND 31),
	postMonth int NOT NULL CHECK(postMonth BETWEEN 1 AND 12),
	postYear int NOT NULL CHECK(postYear > 2015),
	photoText VARCHAR(255) NOT NULL,
	photoURL VARCHAR(255) NOT NULL,
	userID int NOT NULL,
	FOREIGN KEY (userID) REFERENCES User(userID),
	locationID int NOT NULL,
	FOREIGN KEY (locationID) REFERENCES Location(locationID)
);


CREATE TABLE Comment(
	commentID int NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (commentID),
	commentDay int NOT NULL CHECK(commentDay BETWEEN 1 AND 31),
	commentMonth int NOT NULL CHECK(commentMonth BETWEEN 1 AND 12),
	commentYear int NOT NULL CHECK(commentYear > 2015),
	commentContent VARCHAR(255) NOT NULL,
	userID int NOT NULL,
	FOREIGN KEY (userID) REFERENCES User(userID),
	postID int NOT NULL,
	FOREIGN KEY (postID) REFERENCES Photo(postID),
	FOREIGN KEY (postID) REFERENCES Text(postID)
);
