
INSERT INTO Location VALUES
	(1, 'Basel', 'Schweiz');

INSERT INTO Location VALUES
	(2, 'Gelsenkirchen', 'Deutschland');

INSERT INTO Location VALUES
	(3, 'New York', 'USA');

INSERT INTO Location VALUES
	(4, 'Zürich', 'Schweiz');

INSERT INTO Location VALUES
	(5, 'Bern', 'Schweiz');

INSERT INTO Location VALUES
	(6, 'Lausanne', 'Schweiz');

INSERT INTO Location VALUES
	(7, 'Luzern', 'Schweiz');

INSERT INTO Location VALUES
	(8, 'Berlin', 'Deutschland');

INSERT INTO Location VALUES
	(9, 'München', 'Deutschland');

INSERT INTO Location VALUES
	(10, 'Hamburg', 'Deutschland');

INSERT INTO Location VALUES
	(11, 'Köln', 'Deutschland');

INSERT INTO Location VALUES
	(12, 'Rheinfelden', 'Deutschland');

INSERT INTO Location VALUES
	(13, 'Grenzach-Wyhlen', 'Deutschland');

INSERT INTO Location VALUES
	(14, 'Paris', 'Frankreich');

INSERT INTO Location VALUES
	(15, 'Springfield', 'USA');

INSERT INTO User VALUES
	(1, 'firstuser', 'Muster', 'Max', 1, 1, 1984, 'maxmuster@mail.com', 'mypw', 'm', 'http://ourserver.ch/profilpics/firstuser.jpg', 1, 1);

INSERT INTO User VALUES
	(2, 'hansi1', 'Müller', 'Hans', 23, 8, 1956,'hmueller@patrioten.ch' 'hansim1956', 'm', 'http://ourserver.ch/profilpics/hansi1.jpg', 1, 1);

INSERT INTO User VALUES
	(3,'imParis', 'Hilton', 'Paris', 17, 2, 1981, 'paris@hilton.com', '    ', 'w', 'http://ourserver.ch/profilpics/imParis.jpg', 1, 3);

INSERT INTO User VALUES
	(4,'embolo36', 'Embolo', 'Breel Donald', 14, 2, 1997, 'breeldonaldembolo@gmx.ch', 'allezFCB', 'm', 'http://ourserver.ch/profilpics/embolo36.jpg', 2, 1);

INSERT INTO User VALUES
	(5,'yr94', 'Rümmele', 'Yannick', 26, 5, 1994, 'y.ruemmele@stud.unibas.ch', 'z6472164873216487urwqo', 'm', 'http://ourserver.ch/profilpics/yr94.jpg', 12, 13);

INSERT INTO User VALUES
	(6,'JackTheRipper', 'Jack', 'The Ripper', 17, 2, 1981, 'jtr@jack.com', '6328719658743165407316508743165', 'm', '', 6, 6);

INSERT INTO User VALUES
	(7,'DavidGuetta', 'Guetta', 'David', 7, 11, 1967, 'david@fuckmeimfamous.com', '8724361987651875613687165874296540871655780', 'm', 'http://ourserver.ch/profilpics/DavidGuetta.jpg', 14, 14);

INSERT INTO User VALUES
	(8,'bart', 'Simpson', 'bart', 1, 4, 1979, 'bartsimpson@gmail.com', '76321875614387658716931', 'm', 'http://ourserver.ch/profilpics/bart.jpg', 15, 15);

INSERT INTO User VALUES
	(9,'moststupidmanalive', 'Trump', 'Donald', 14, 6, 1946, 'trump@dummie.com', '7328904781275182', 'm', 'http://ourserver.ch/profilpics/moststupidmanalive.jpg', 3, 3);

INSERT INTO User VALUES
	(10,'AngieGermany', 'Merkel', 'Angela', 17, 7, 1954, 'kanzlerin@cdu.de', '86874632178678687', 'w', 'http://ourserver.ch/profilpics/AngieGermany.jpg', 10, 8);

INSERT INTO User VALUES
	(11,'sarahlombardi92', 'Lombardi', 'Sarah', 15, 10, 1992, 'sarahl92@gmx.de', '2347898715618671', 'w', 'http://ourserver.ch/profilpics/sarahlombarti92.jpg', 11, 11);

INSERT INTO Friendship VALUES
  (1,1,2);

INSERT INTO Friendship VALUES
  (2,1,3);

INSERT INTO Friendship VALUES
  (3,1,4);

INSERT INTO Friendship VALUES
  (4,1,4);

INSERT INTO Friendship VALUES
  (5,2,4);

INSERT INTO Friendship VALUES
  (6,5,1);

INSERT INTO Friendship VALUES
  (7,1,5);

INSERT INTO Friendship VALUES
  (8,6,1);

INSERT INTO Friendship VALUES
  (9,1,7);

INSERT INTO Friendship VALUES
  (10,11,4);

INSERT INTO Friendship VALUES
  (11,1,8);

INSERT INTO Friendship VALUES
  (12,1,9);

INSERT INTO Friendship VALUES
  (13,1,10);

INSERT INTO Friendship VALUES
  (14,1,11);

INSERT INTO Friendship VALUES
  (15,10,11);

INSERT INTO Text VALUES
	(1, 8, 11, 2016, 'Reha läuft...', 4, 2);

INSERT INTO Text VALUES
	(2, 8, 11, 2016, 'wir schaffen das', 10, 8);

INSERT INTO Text VALUES
	(3, 8, 11, 2016, 'make America great again', 9, 3);

INSERT INTO Text VALUES
	(4, 10, 11, 2016, 'Hopp Blau-Weiss', 4, 2);

INSERT INTO Text VALUES
	(5, 15, 11, 2016, 'Test', 5, 1);

INSERT INTO Photo VALUES
	(6, 10, 12, 2016, 'Gruesse aus New York', 'http://ourserver.ch/pics/2.jpg', 2, 3);

INSERT INTO Text VALUES
	(7, 10, 12, 2016, 'Test nach Foto', 5, 1);
