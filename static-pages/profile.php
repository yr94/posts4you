<?php
session_start();
?>


<!-- MAKE FULLWIDTH -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>POSTS 4U</title>
    <link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="profile.php">POSTS 4U</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="profile.php">Profile</a></li>
                <!-- <li><a href="#">Profile</a></li> -->
                <li><a href="search.php">Search</a></li>
                <li><a href="friends.php">Friends</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="settings.php">Settings</a></li>
                <!-- <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li> -->
                <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
            </ul>
        </div>
    </div>
</nav>

<?php

$servername = "localhost";
$dbusername = "root";
$dbpassword = "root";
$dbname = "posts4u";
if ($_SESSION["username"] != ""){
    $username = $_SESSION["username"];
} else {
    echo '<script type="text/javascript">';
    echo 'window.location = "login.php"';
    echo '</script>';
}

echo "<br>";

$conn = new mysqli($servername, $dbusername, $dbpassword, $dbname);



if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} else {
    //echo "ready";
}

$sql = "SELECT * FROM User WHERE username = '$username'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $firstname = $row["firstName"];
        $lastname = $row["lastName"];
        $email = $row["eMail"];
        $bday = $row["birthDay"];
        $bmonth = $row["birthMonth"];
        $byear = $row["birthYear"];
        $_SESSION["firstName"] = $firstname;
        $_SESSION["lastName"] = $lastname;

      //  echo "id: " . $row["id"]. " - Name: " . $row["firstname"]. " " . $row["lastname"]. "<br>";
    }
} else {
    echo "0 results";
}
$conn->close();
?>



<!- profile container ->
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-">
            <div class="well well-sm">
                <div class="row">
                    <!-- <div class="col-sm-6 col-md-4">
                        <img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive" />
                    </div> -->
                    <div class="col-sm-6 col-md-8">
                        <h4>
                            <?php
                                echo $firstname;
                                echo " ";
                                echo $lastname;
                            ?></h4>
                        <p>
                            <i class="glyphicon glyphicon-user"></i>
                            <?php
                                echo $username;
                            ?>
                            <br />
                            <i class="glyphicon glyphicon-map-marker"></i> Basel, Switzerland
                            <br />
                            <i class="glyphicon glyphicon-envelope"></i> <?php echo $email; ?>
                            <br />
                            <i class="glyphicon glyphicon-gift"></i>
                            <?php
                            $months = array (1=>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');
                            echo $months[(int)$bmonth];
                            echo " ";
                            echo $bday;
                            echo ", ";
                            echo $byear;
                            ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-">
            <div class="well well-sm">
                <h4>How about a new post?</h4>
                <form class = form-inline method = "post">
                    <div class="form-group">
                                        <input type="text" class="form-control" name="postcontent" placeholder="">


                                        <button type="submit" class="btn btn-default">Post!</button>


                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$servername = "localhost";
$dbusername = "root";
$dbpassword = "root";
$dbname = "posts4u";
$userID = $_SESSION["userID"];

$conn = new mysqli($servername, $dbusername, $dbpassword, $dbname);



if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} else {
    //echo "ready";
}


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $postcontent = $_POST["postcontent"];
    if(!empty($postcontent))   //checking the 'user' name which is from Sign-In.html, is it empty or have some text
    {
        $sql = "INSERT INTO Text VALUES (DEFAULT, DAY(CURRENT_DATE), MONTH(CURRENT_DATE), YEAR(CURRENT_DATE), '$postcontent', $userID, 1)";

        if ($conn->query($sql) === TRUE) {
            $page = $_SERVER['PHP_SELF'];
            echo '<meta http-equiv="Refresh" content="0;' . $page . '">';
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
        $conn->close();



    }
}

?>


<?php
//get all posts and put them on the wall
$servername = "localhost";
$dbusername = "root";
$dbpassword = "root";
$dbname = "posts4u";
$username = $_SESSION["username"];
$userID = $_SESSION["userID"];
//echo "<br>";

$conn = new mysqli($servername, $dbusername, $dbpassword, $dbname);



if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} else {
    //echo "ready";
}

$sql = "SELECT * FROM Text WHERE userID = '$userID' ORDER BY postID DESC";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $content = $row["textContent"];
        $day = $row["postDay"];
        $month = $row["postMonth"];
        $year = $row["postYear"];
        $locationID = $row["locationID"];
        $months = array (1=>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');
        $monthText = $months[(int)$month];



        //$sql = "SELECT city, country FROM Location WHERE locationID = '$locationID'";
        //$result = $conn->query($sql);

            echo '</div>';
                echo '<div class="container">';
                echo '<div class="row">';
                echo '<div class="col-xs-12 col-sm-6 col-">';
                echo '<div class="well well-sm">';
                echo '<h4 class="btn-link">';
                echo $firstname;
                echo ' ';
                echo $lastname;
                echo '</h4>';
                echo '<h6>Posted on ';
                echo $day;
                echo ' ';
                echo $monthText;
                echo '<br />';
                echo '<br>';
                echo '<h5>';
                echo $content;
                echo '</h5>';

                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';
        echo '</div>';
        echo '</div>';
        echo '</div>';
        echo '</div>';
        echo '</div>';
        echo '</div>';
        echo '</div>';

            }

        //$conn->close();



} else {
    echo "no posts found";
}
$conn->close();
?>


<!--
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-">
            <div class="well well-sm">
                <h4 class="btn-link">Your Friend</h4>
                <h6>Posted from Basel, on 11 November at 2:00</h6>

                <br />
                <h5>Hey! Great seeing you this weekend!</h5>
                <div>
                    <img src="http://placehold.it/300x225" alt="" class="img-rounded img-responsive" />
                </div>

            </div>
        </div>
    </div>
</div>
-->




</body>
