<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>POSTS 4U</title>
    <link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="profile.php">POSTS 4U</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="profile.php">Profile</a></li>
                <!-- <li><a href="#">Profile</a></li> -->
                <li class="active"><a href="#">Search</a></li>
                <li><a href="friends.php">Friends</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="settings.php">Settings</a></li>
                <!-- <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li> -->
                <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
            </ul>
        </div>
    </div>
</nav>



<div class="container">
    <h2>Search</h2>
    <form>
        <!--<div class="search-parameter">
            <input type="email" class="form-control" id="email" placeholder="New Search">
        </div> -->

        <article>
            <fieldset>
                <br>
                <form>
                    <label><input value="1" id="1" type="radio" name="formselector"
                                  onclick="displayForm(this)">Search by Poster</label>
                    <br>
                    <label><input value="2" id="2" type="radio" name="formselector"
                                  onclick="displayForm(this)">Search by Posts</label>
                </form>

                <form style="display:none" id="posterform"><label></label>
                    <br>
                    <br>
                    <dd><p>Username: <input type="text" id="posterusername"
                                                     name="posterusername" value=""></p>
                        <p>Name: <input type="text" id="postername"
                                            name="postername" value=""></p>
                        <p>Email: <input type="email" id="posteremail"
                                            name="posteremail" value=""></p>

                        <p>Location: <input type="text"
                                                       id="posterlocation" name="posterlocation" value=""></p>
                        <p>Birthday:<select
                                name="posterbirthdate" required>
                            <option value="-">-</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>


                        </select>
                            <span><select
                                    name="posterbirthmonth" required>
                            <option value="-">-</option>
                            <option value="1">01</option>
                            <option value="2">02</option>
                            <option value="3">03</option>
                            <option value="4">04</option>
                            <option value="5">05</option>
                            <option value="6">06</option>
                            <option value="7">07</option>
                            <option value="8">08</option>
                            <option value="9">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select></span>
                            <span><select name="posterbirthyear" required>
                            <option value="-">-</option>
                            <option value="2000">2000</option>
                            <option value="1999">1999</option>
                            <option value="1998">1998</option>
                            <option value="1997">1997</option>
                            <option value="1996">1996</option>
                            <option value="1995">1995</option>
                            <option value="1994">1994</option>
                            <option value="1993">1993</option>
                            <option value="1992">1992</option>
                            <option value="1991">1991</option>
                            <option value="1990">1990</option>
                            <option value="1989">1989</option>
                            <option value="1988">1988</option>
                            <option value="1987">1987</option>
                            <option value="1986">1986</option>
                            <option value="1985">1985</option>
                            <option value="1984">1984</option>
                            <option value="1983">1983</option>
                            <option value="1982">1982</option>
                            <option value="1981">1981</option>
                            <option value="1980">1980</option>
                            <option value="1979">1979</option>
                            <option value="1978">1978</option>
                            <option value="1977">1977</option>
                            <option value="1976">1976</option>
                            <option value="1975">1975</option>
                            <option value="1974">1974</option>
                            <option value="1973">1973</option>
                            <option value="1972">1972</option>
                            <option value="1971">1971</option>
                            <option value="1970">1970</option>
                            <option value="1969">1969</option>
                            <option value="1968">1968</option>
                            <option value="1967">1967</option>
                            <option value="1966">1966</option>
                            <option value="1965">1965</option>
                            <option value="1964">1964</option>
                            <option value="1963">1963</option>
                            <option value="1962">1962</option>
                            <option value="1961">1961</option>
                            <option value="1960">1960</option>
                            <option value="1959">1959</option>
                            <option value="1958">1958</option>
                            <option value="1957">1957</option>
                            <option value="1956">1956</option>
                            <option value="1955">1955</option>
                            <option value="1954">1954</option>
                            <option value="1953">1953</option>
                            <option value="1952">1952</option>
                            <option value="1951">1951</option>
                            <option value="1950">1950</option>
                            <option value="1949">1949</option>
                            <option value="1948">1948</option>
                            <option value="1947">1947</option>
                            <option value="1946">1946</option>
                            <option value="1945">1945</option>
                            <option value="1944">1944</option>
                            <option value="1943">1943</option>
                            <option value="1942">1942</option>
                            <option value="1941">1941</option>
                            <option value="1940">1940</option>
                            <option value="1939">1939</option>
                            <option value="1938">1938</option>
                            <option value="1937">1937</option>
                            <option value="1936">1936</option>
                            <option value="1935">1935</option>
                            <option value="1934">1934</option>
                            <option value="1933">1933</option>
                            <option value="1932">1932</option>
                            <option value="1931">1931</option>
                            <option value="1930">1930</option>
                            <option value="1929">1929</option>
                            <option value="1928">1928</option>
                            <option value="1927">1927</option>
                            <option value="1926">1926</option>
                            <option value="1925">1925</option>
                            <option value="1924">1924</option>
                            <option value="1923">1923</option>
                            <option value="1922">1922</option>
                            <option value="1921">1921</option>
                            <option value="1920">1920</option>
                            <option value="1919">1919</option>
                            <option value="1918">1918</option>
                            <option value="1917">1917</option>
                            <option value="1916">1916</option>
                            <option value="1915">1915</option>
                            <option value="1914">1914</option>
                            <option value="1913">1913</option>
                            <option value="1912">1912</option>
                            <option value="1911">1911</option>
                            <option value="1910">1910</option>
                            <option value="1909">1909</option>
                            <option value="1908">1908</option>
                            <option value="1907">1907</option>
                            <option value="1906">1906</option>
                            <option value="1905">1905</option>
                            <option value="1904">1904</option>
                            <option value="1903">1903</option>
                            <option value="1902">1902</option>
                            <option value="1901">1901</option>
                            <option value="1900">1900</option>
                </select></span>
                    </dd>
                </form>
                <form style="display:none" id="postsform"><label></label>
                    <br>
                    <br>
                    <dd><p>Poster: <input type="text" id="poster"
                                            name="poster" value=""></p>

                        <p>Location: <input type="text"
                                            id="post-location" name="post-location" value=""></p>
                        <p>Keywords: <input type="text"
                                            id="post-keywords" name="post-keywords" value=""></p>
                        <p>From: <select
                                name="fromdate" required>
                            <option value="-">-</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                            <option value="13">13</option>
                            <option value="14">14</option>
                            <option value="15">15</option>
                            <option value="16">16</option>
                            <option value="17">17</option>
                            <option value="18">18</option>
                            <option value="19">19</option>
                            <option value="20">20</option>
                            <option value="21">21</option>
                            <option value="22">22</option>
                            <option value="23">23</option>
                            <option value="24">24</option>
                            <option value="25">25</option>
                            <option value="26">26</option>
                            <option value="27">27</option>
                            <option value="28">28</option>
                            <option value="29">29</option>
                            <option value="30">30</option>
                            <option value="31">31</option>


                        </select>
                            <span><select
                                    name="frommonth" required>
                            <option value="-">-</option>
                            <option value="1">01</option>
                            <option value="2">02</option>
                            <option value="3">03</option>
                            <option value="4">04</option>
                            <option value="5">05</option>
                            <option value="6">06</option>
                            <option value="7">07</option>
                            <option value="8">08</option>
                            <option value="9">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select></span>
                            <span><select name="fromyear" required>
                            <option value="-">-</option>
                            <option value="2015">2015</option>
                            <option value="2016">2016</option>
                        </select></span>

                    </dd>

                    <p>To: <select <!--the dd was a p -->
                            name="todate" required>
                        <option value="-">-</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                        <option value="31">31</option>


                    </select>
                        <span><select
                                name="tomonth" required>
                            <option value="-">-</option>
                            <option value="1">01</option>
                            <option value="2">02</option>
                            <option value="3">03</option>
                            <option value="4">04</option>
                            <option value="5">05</option>
                            <option value="6">06</option>
                            <option value="7">07</option>
                            <option value="8">08</option>
                            <option value="9">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select></span>
                        <span><select name="toyear" required>
                            <option value="-">-</option>
                            <option value="2015">2015</option>
                            <option value="2016">2016</option>

                        </select></span>

                            </p>
                </form>

                <br>
            </fieldset>
        </article>

        <script type="text/javascript">
            function displayForm(c){
                if(c.value == "1"){

                    document.getElementById("posterform").style.display='inline';
                    document.getElementById("postsform").style.display='none';
                }
                else if(c.value =="2"){
                    document.getElementById("posterform").style.display='none';

                    document.getElementById("postsform").style.display='inline';
                }
                else{
                }
            }


        </script>
        <button type="submit" class="btn btn-default" onclick="showResult()">Submit</button>
        <script>
            function showResult() {
                if (document.getElementById("postsform").style.display == 'none'){
                    document.getElementById("posterresults").style.display == 'inline'
                }

            }
        </script>
    </form>
</div>
<div style="display:none" id="posterresults">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-">
                <div class="well well-sm">
                    <h4 class="btn-link">Donald Trump</h4>
                    <h6>Posted from Basel, on 11 November at 4:00</h6>
                    <br />
                    <h5>Post</h5>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-">
                <div class="well well-sm">
                    <h4 class="btn-link">Your Friend</h4>
                    <h6>Posted from Basel, on 11 November at 2:00</h6>

                    <br />
                    <h5>Hey! Great seeing you this weekend!</h5>
                    <div>
                        <img src="http://placehold.it/300x225" alt="" class="img-rounded img-responsive" />
                    </div>

                </div>
            </div>
        </div>
    </div>





</body>
