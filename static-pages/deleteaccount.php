<?php
session_start();
$username = $_SESSION["username"];
$userID = $_SESSION["userID"];

$servername = "localhost";
$dbusername = "root";
$dbpassword = "root";
$dbname = "posts4u";


// Create connection
$conn = new mysqli($servername, $dbusername, $dbpassword, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql1 = "DELETE FROM Text WHERE userID = '$userID'";

if ($conn->query($sql1) === TRUE) {
    echo "Record deleted successfully";
} else {
    echo "Error deleting record: " . $conn->error;
}

$sql = "DELETE FROM User WHERE username = '$username'";

if ($conn->query($sql) === TRUE) {
    echo "Record deleted successfully";
} else {
    echo "Error deleting record: " . $conn->error;
}

$conn->close();

session_destroy();
echo '<script type="text/javascript">';
echo 'window.location = "login.php"';
echo '</script>';
?>