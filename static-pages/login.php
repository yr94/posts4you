<?php
session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>POSTS 4U</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <script src="js/jquery-3.1.1.js"></script>
</head>

<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">POSTS 4U</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="signup.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            </ul>
        </div>
    </div>
</nav>

<?php
$servername = "localhost";
$dbusername = "root";
$dbpassword = "root";
$dbname = "posts4u";

$conn = new mysqli($servername, $dbusername, $dbpassword, $dbname);



if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} else {
    echo "ready";
}


    session_start();   //starting the session for user profile page

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST["username"];
    $password = $_POST["password"];
    if(!empty($username))   //checking the 'user' name which is from Sign-In.html, is it empty or have some text
    {
        $sql = "SELECT password FROM User WHERE username = '$username'";
        $result = $conn->query($sql);
        $encryptedloginpassword = md5($password);

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                if (($encryptedloginpassword == $row["password"])){
                    echo "<br>";
                    echo "success!";
                    $_SESSION["username"] = "$username";

                    $sql = "SELECT userID FROM User WHERE username = '$username'";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                            $_SESSION["userID"] = $row["userID"];
                        }
                    }

                    echo '<script type="text/javascript">';
                    echo 'window.location = "profile.php"';
                    echo '</script>';
                    //echo $_SESSION["username"];


                } else {
                    echo "<br>";
                    echo "wrong password!";

                }
            }


        } else {
            echo "no user with this username exists!";
        }
        $conn->close();



    }
}

?>

<div class="container">
    <h2>Log In!</h2>
    <form class="form-horizontal" method="post">
        <div class="form-group">
            <label class="control-label col-sm-3" for="username">Username:</label>
            <div class="col-xs-4">
                <input type="text" class="form-control" name="username" placeholder="Username">
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="password">Password:</label>
            <div class="col-xs-4">
                <input type="password" class="form-control" name="password" placeholder="Password">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-10">
                <button type="submit" class="btn btn-default">Log In!</button>
            </div>
        </div>
    </form>
</div>

<?php
echo $usernameerr;
?>



</body>
</html>