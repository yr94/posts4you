
<!-- MAKE FULLWIDTH -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>POSTS 4U</title>
    <link rel="stylesheet" href="css/bootstrap.css">
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="profile.php">POSTS 4U</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="profile.php">Profile</a></li>
                <!-- <li><a href="#">Profile</a></li> -->
                <li><a href="search.php">Search</a></li>
                <li class="active"><a href="friends.php">Friends</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="settings.php">Settings</a></li>
                <!-- <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li> -->
                <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Log out</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive" />
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <h4>
                            Friend 3</h4>
                        <p>
                            <i class="glyphicon glyphicon-user"></i> username
                            <br />
                            <i class="glyphicon glyphicon-map-marker"></i> Basel, Switzerland
                            <br />
                            <i class="glyphicon glyphicon-envelope"></i> email@youremail.com
                            <br />
                            <i class="glyphicon glyphicon-gift"></i> July 01, 1994</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive" />
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <h4>
                            Friend 2</h4>
                        <p>
                            <i class="glyphicon glyphicon-user"></i> username
                            <br />
                            <i class="glyphicon glyphicon-map-marker"></i> Basel, Switzerland
                            <br />
                            <i class="glyphicon glyphicon-envelope"></i> email@youremail.com
                            <br />
                            <i class="glyphicon glyphicon-gift"></i> July 01, 1994</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive" />
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <h4>
                            Friend 1</h4>
                        <p>
                            <i class="glyphicon glyphicon-user"></i> username
                            <br />
                            <i class="glyphicon glyphicon-map-marker"></i> Basel, Switzerland
                            <br />
                            <i class="glyphicon glyphicon-envelope"></i> email@youremail.com
                            <br />
                            <i class="glyphicon glyphicon-gift"></i> July 01, 1994</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <img src="http://placehold.it/380x500" alt="" class="img-rounded img-responsive" />
                    </div>
                    <div class="col-sm-6 col-md-8">
                        <h4>
                            Paris Hilton</h4>
                        <p>
                            <i class="glyphicon glyphicon-user"></i> pari$
                            <br />
                            <i class="glyphicon glyphicon-map-marker"></i> Paris, France
                            <br />
                            <i class="glyphicon glyphicon-envelope"></i> paris@parishilton.com
                            <br />
                            <i class="glyphicon glyphicon-gift"></i> July 01, 1994</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



</body>
