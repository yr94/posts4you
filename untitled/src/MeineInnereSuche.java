import org.xml.sax.Locator;

public class MeineInnereSuche{
    class InnerClass implements Locator{
        @Override
        public String getPublicId() {
            return null;
        }

        @Override
        public String getSystemId() {
            return null;
        }

        @Override
        public int getLineNumber() {
            return 0;
        }

        @Override
        public int getColumnNumber() {
            return 0;
        }
    }

    public InnerClass erstelleFehler(){
        return new InnerClass();
    }
}